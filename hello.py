from flask import Flask,render_template,request, redirect, url_for
from flask import request, abort, flash, session

import threading
import asyncio
import logging
import sys
import time

import veilid

LOG = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

app = Flask(__name__)


@app.route('/')
def hello():
    return redirect(url_for('form'))

@app.route('/explorer')
def explorer():

    return render_template('explorer.html')

@app.route('/exploredht', methods = ['POST', 'GET'])
def explorer_data():
    if request.method == 'GET':
        return f"The URL /data is accessed directly. Try to /explorer"

    if request.method == 'POST':
        form_data = request.form

        if 'VLD0:' in form_data['dht_key']:
            key = form_data['dht_key'].split(':')[1]
        else:
            key = form_data['dht_key']

        loop = asyncio.get_event_loop()
        result = loop.run_until_complete(get_value('localhost', 5959, key))
        value = result

        if value == 'error':
            return render_template('norecord.html', key=key)
        else:
            return render_template('dhtrecord.html', key=key, value=value)

@app.route('/search/<key>')
def explorer_key(key=None):

    if 'VLD0:' in key:
        key = key.split(':')[1]

    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(get_value('localhost', 5959, key))

    if result == 'error':
        return render_template('error.html')

    value = result

    if value == 'error':
        return render_template('norecord.html', key=key)
    else:
        return render_template('dhtrecord.html', key=key, value=value)


@app.route('/form')
def form():

    return render_template('form.html')

@app.route('/data', methods = ['POST', 'GET'])
def data():
    if request.method == 'GET':
        return f"The URL /data is accessed directly. Try to /form"

    if request.method == 'POST':
        form_data = request.form
        LOG.info(form_data)

        loop = asyncio.get_event_loop()
        result = loop.run_until_complete(insert_value('localhost', 5959, form_data['data']))

        if result == 'error':
            return render_template('error.html')

        LOG.info(result)
        if 'VLD0:' in result:
            inserted_key = result.split(':')[1]
        else:
            inserted_key = result

        return render_template('success.html', data=form_data['data'], key=inserted_key)

#####

async def noop_callback(*args, **kwargs):
    return

async def keygen(host: str, port: int):
    """Generate a keypair."""

    LOG.info(f"keygen:{host=}, {port=}")

    LOG.debug(f"keygen:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")
    try:
        conn = await veilid.json_api_connect(host, port, noop_callback)
    except:
        return 'error'

    LOG.debug("keygen:Getting a crypto system")
    crypto_system = await conn.get_crypto_system(veilid.CryptoKind.CRYPTO_KIND_VLD0)
    async with crypto_system:
        LOG.debug("keygen:Generating a keypair")
        my_keypair = await crypto_system.generate_key_pair()
        LOG.debug(f"keygen:Got {my_keypair=}")

    await conn.release()
    return my_keypair

async def insert_value(host: str, port: int, name: str):
    LOG.info(f"start:{host=}, {port=}, {name=}")

    LOG.debug(f"start:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")

    try:
        my_keypair = await keygen(host, port)
    except:
        return 'error'

    try:
        conn = await veilid.json_api_connect(host, port, noop_callback)
    except:
        return 'error'

    LOG.info(f"Your public key is: {my_keypair.key()}")

    LOG.debug("start:Opening a private routing context")
    router = await (await conn.new_routing_context()).with_privacy()

    try:
        async with router:

            LOG.debug("start:Creating a new DHT record")
            my_loc = await router.create_dht_record(veilid.DHTSchema.dflt(2))

            LOG.info(f"New chat key: {my_loc.key}")

            LOG.debug("start:Set DHT record")
            result= await router.set_dht_value(my_loc.key, 0, name.encode())

            await router.close_dht_record(my_loc.key)

        await conn.release()
        return my_loc.key
    except:
        return 'error'

async def get_value(host: str, port: int, key: str):
    LOG.info(f"start:{host=}, {port=}, {key=}")

    LOG.debug(f"start:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")
    try:
        conn = await veilid.json_api_connect(host, port, noop_callback)
    except:
        return 'error'

    LOG.debug("start:Opening a private routing context")
    router = await (await conn.new_routing_context()).with_privacy()

    try:
        async with router:

            await router.open_dht_record(key, None)

            result = await router.get_dht_value(key, 0, False)
            LOG.info(result)
            await router.close_dht_record(key)

        await conn.release()
        return result.data.decode()
    except:
        return 'error'

if __name__ == '__main__':
    asyncio.set_event_loop(asyncio.new_event_loop())
    app.run(port=8000)

