# vpasta.lol


## What is it?

Its a (very) simple pastebin clone which stores plain-text in the Veilid DHT, the site creates a unique key for each entry which would then allow the text to be accessed from any Veilid node.

## Setup

 * veilid-server running on a small VPS
 * python3.11 / flask

## Whats next?

  * Its a terrible design and only just works so ideally would be good to make it a bit less dirty
  * Add some spam limits
  * Create a proper webapp using the veilid WASM implementation

## References

  * A fair bit of code was borrowed from the python demo (https://gitlab.com/veilid/python-demo) and tests (https://gitlab.com/veilid/veilid/-/tree/main/veilid-python/tests) from the veilid teams
